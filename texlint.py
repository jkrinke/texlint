#!/usr/bin/python

# texlint.py - A tool to check for simple LaTeX layout problems.

# See <https://bitbucket.org/jkrinke/texlint> for details.

import sys
import re

print """texlint.py is checking your sources..."""

rules = [
    (re.compile(r"\s---|---\s"),
     """Found whitespace around `---'.
    Please either use `--' instead (british english, prefered), or
    remove the whitespace around `---'.
    <http://tex.stackexchange.com/questions/3819/dashes-vs-vs>"""),
    (re.compile(r'_\{[A-Za-z]{2,}|\$[A-Za-z]{2,}'),
    """It seems that words are used in math mode.
    A word XYZ in math mode actually means X times Y times Z and will cause
    weird spacing. The word should be enclosed in \\mathrm, \\mathit, \\textit,
    or \\textrm.

    Moreover, variables should always be single-letter in italics. Textual sub-
    and superscripts should not be italics."""),
    (re.compile(r"i\.e\.\s"),
     """Found whitespace after i.e.
    Please use a fixed space `\ ' instead.
    <http://tex.stackexchange.com/questions/2734/taking-unncessary-space-after-e-g-or-i-e>"""),
    (re.compile(r"e\.g\.\s"),
     """Found whitespace after e.g.
    Please use a fixed space `\ ' instead.
    <http://tex.stackexchange.com/questions/2734/taking-unncessary-space-after-e-g-or-i-e>"""),
    (re.compile(r"[^%] \\(cite|ref)"),
     """Found whitespace before \\cite or \\ref.
    Please use a non-breaking space `~' instead.
    <http://tex.stackexchange.com/questions/9633/why-should-i-put-a-before-ref-or-cite>"""),
    (re.compile(r"\([a-c1-3]\)\s"),
     """Found whitespace after an ad-hoc inline list item.
    Please use a non-breaking space `~' instead.
    <http://tex.stackexchange.com/questions/5511/good-practice-on-spacing>"""),
    (re.compile(r"\\emph\{et al"),
    """Found et al. in italics.
    Commonly used Latin words and abbreviations should not be italicized.
    <http://english.stackexchange.com/questions/180515/should-et-al-be-in-italics>""")
    ]

def location2line(content, location):
    return len(content[:location].splitlines())

for name in sys.argv[1:]:
    
    with open(name, "r") as file:
        content = file.read()

    # General rules
    for (p, t) in rules:
        m = p.search(content)
        if m:
            print name, ':', t

        for m in p.finditer(content):
            print name + ":" + str(location2line(content, m.start())) + ": >" + m.group() + "<"

    # Special rules
    p = re.compile(r"\\usepackage.*\{listings\}")
    if p.search(content):
        if not re.search(r"stringstyle", content):
            print name + ": use of `listings' package detected."
            print """
    If you use the `listings' package, you should not use the default.
    Please make sure to set your listings in \\ttfamily."""



