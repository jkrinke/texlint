texlint.py

While advising students on commom layout pitfalls in LaTeX, I decided it is time to write a tool for it. Texlint.py is a simple Python script that reads a list of LaTeX files and checks for common problems in it. It usually tries to give advise on why it is considered to be an issue.

I would be happy to hear from you if you find this script helpful.

This project is hosted at <https://bitbucket.org/jkrinke/texlint>.

Jens Krinke <krinke@acm.org>

"It will not work."


Resources

I am usually relying on various sources as guidance in style questions.  Here are a few resources that you may find helpful (in no particual order).

* Mignon Fogarty is Grammar Girl and her articles are an extremely valuable resource.
<http://www.quickanddirtytips.com/grammar-girl>

* English Language & Usage Stack Exchange is very useful if you serach for LaTeX related questions.
<http://english.stackexchange.com/search?q=latex>

* Andreas Zeller's top ten presentation issues in other's papers
<http://andreas-zeller.blogspot.co.uk/2013/04/my-top-ten-presentation-issues-in.html>
